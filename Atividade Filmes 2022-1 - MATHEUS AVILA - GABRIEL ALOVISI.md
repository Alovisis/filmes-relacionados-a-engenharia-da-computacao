# Atividade sobre Filmes relacionados a Engenharia da Computação.

![](https://upload.wikimedia.org/wikipedia/pt/thumb/1/1a/O_Jogo_da_Imitação.jpg/230px-O_Jogo_da_Imitação.jpg)

O filme **“O Jogo da Imitação”** inspirado na biografia de **“Alan Turing: The Enigma, de Andrew Hodges”**, conta **a história verídica de Alan Turing**, um humano com habilidades superiores à média, um gênio da matemática e criptoanalista, que foi recrutado no ano de 1939 em plena II Guerra Mundial pelo governo britânico para um projeto secreto, com o objetivo de desenvolver uma máquina capaz de decifrar e quebrar os códigos alemães do Enigma, máquina com a qual os nazistas enviavam mensagens de táticas de combates. A máquina Enigma possuía mensagens criptografadas com informações sobre a localização das tropas nazistas e onde seriam os próximos ataques. Sendo essas mensagens que eram emitidas por ondas de rádio. O objetivo era complexo além de todas as complicações os nazistas mudavam todos os dias suas combinações, assim sendo mais difícil a decifração.

Turing tenta desenvolver e resolver o problema, criando outra máquina junto a sua equipe altamente capacitada e aliados para quebrar o código produzido pela máquina Enigma, uma máquina com o intuito de desenvolver e pensar como um ser humano, como um cérebro elétrico, um dispositivo lógico que ele chamou de "automatic machine”, capaz de ler, escrever e apagar símbolos binários em uma fita de comprimento ilimitado e dividida por quadrados de igual tamanho, assim descriptografando as mensagens, e contribuindo na época com o desenvolvimento de novas invenções tecnológicas, conquistando o objetivo, após desvendar os códigos Turing se torna herói, ao obter para o seu exército uma vantagem sobre os alemães e posteriormente decorrendo o fim da Segunda Guerra Mundial.

![](https://2.bp.blogspot.com/-tBXmTg9GWSA/VfilFB1AYJI/AAAAAAAAH4k/yznX6B7AlIg/s1600/filme-o-jogo-da-imitacao-the-game-imitation-2014-testeievoce-2.jpg)

Alan Turing é considerado o  **pai da computação**, sendo principal no desenvolvimento da ciência da computação, tendo o ponto fundamental no âmbito da criptografia para invenção da máquina de decodificação, contribuindo muito na época com o desenvolvimento e aprimoramento de **invenções anteriores**, sendo essencial para as  **invenções futuras** que presenciamos nos tempos de hoje, como todos os sistemas de informação, evolução do rádio, métodos de criptografia para segurança de dados utilizados, redes sociais. Assim contribuindo muito com a história, no desenvolvimento da humanidade e das novas tecnologias no decorrer dos anos, principalmente da área da computação, tendo **relação fundamental com a Engenharia De Computação**.

![](https://cdn.pensador.com/img/frase/al/an/alan_turing_nos_so_podemos_ver_um_pouco_do_futuro_mas_o_l9e116o.jpg)

**Referencias:**

MORTEN TYLDUM. ANDREW HODGES. BLACK BEAR PICTURES. O JOGO DA IMITAÇÃO. ALAN TURING: THE ENIGMA. BIOGRAFIA, DRAMA, COMPUTAÇÃO. 28 DE SETEMBRO DE 2014. 

ALAN TURING. “PENSADOR. Disponível em:  https://www.pensador.com/autor/alan_turing/.
